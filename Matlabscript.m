syms y3 dL x r
%Curved portion
y3 = -.01*(x-313)^2+232
dL = sqrt((diff(y3))^2+1) %diff(equation) returns the derivative of an equation
%r is x coord g is y coord
r= vpaintegral((x*dL),280.5845,450)/vpaintegral((dL),280.5845,450) %vpaintegral(equation, lower bound, upper bound) returns the numeric value of a definite integral 
g= vpaintegral((y3*dL),280.5845,450)/vpaintegral((dL),280.5845,450)

syms y2 
%Straight lift Portion 
y2 = 0.839*x-(849/61)
dL2 = sqrt((diff(y2))^2+1)
%r2 is x coord g2 is y coord
r2= vpaintegral((x*dL2),35.659,280.5845)/vpaintegral((dL2),35.659,280.5845)
g2= vpaintegral((y2*dL2),35.659,280.5845)/vpaintegral((dL2),35.659,280.5845)

syms y1 
%Straight start Portion 
y1 = 16
dL3 = sqrt((diff(y2))^2+1)
%r3 is x coord g3 is y coord
r3= vpaintegral((x*dL3),0,35.659)/vpaintegral((dL3),0,35.659)
g3= vpaintegral((y1*dL3),0,35.659)/vpaintegral((dL3),0,35.659)

%Total centroid
T=(r*vpaintegral((dL),273,428))+(r2*vpaintegral((dL2),35.659,273))+(r3*vpaintegral((dL3),0,35.659))
L=(vpaintegral((dL),273,428))+vpaintegral((dL2),35.659,273)+vpaintegral((dL3),0,35.659)

xbar=T/L

T=(g*vpaintegral((dL),273,428))+(g2*vpaintegral((dL2),35.659,273))+(g3*vpaintegral((dL3),0,35.659))
L=(vpaintegral((dL),273,428))+vpaintegral((dL2),35.659,273)+vpaintegral((dL3),0,35.659)

ybar=T/L

%print out all of the values for use
fprintf('%s,%s\n',r, g)
fprintf('%s,%s\n',r2, g2)
fprintf('%s,%s\n',r3, g3)
fprintf('%s,%s\n',xbar, ybar)
